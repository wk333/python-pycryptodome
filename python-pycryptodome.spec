%global _empty_manifest_terminate_build 0
%global pypi_name pycryptodome

Name:		python-pycryptodome
Version:	3.19.1
Release:	1
Summary:	Cryptographic library for Python
License:	BSD-2-Clause and Public Domain and Apache-2.0
URL:		https://www.pycryptodome.org
Source0:	https://files.pythonhosted.org/packages/source/p/pycryptodome/pycryptodome-%{version}.tar.gz

%description
PyCryptodome is a self-contained Python package of low-level
cryptographic primitives.
It supports Python 2.6 and 2.7, Python 3.4 and newer, and PyPy.
You can install it with::
    pip install pycryptodome
All modules are installed under the ``Crypto`` package.
Check the pycryptodomex_ project for the equivalent library that
works under the ``Cryptodome`` package.
PyCryptodome is a fork of PyCrypto. It brings several enhancements
with respect to the last official version of PyCrypto (2.6.1),
for instance:
* Authenticated encryption modes (GCM, CCM, EAX, SIV, OCB)
* Accelerated AES on Intel platforms via AES-NI
* First class support for PyPy
* Elliptic curves cryptography (NIST P-256, P-384 and P-521 curves only)
* Better and more compact API (`nonce` and `iv` attributes for ciphers,
  automatic generation of random nonces and IVs, simplified CTR cipher mode,
  and more)
* SHA-3 (including SHAKE XOFs) and BLAKE2 hash algorithms
* Salsa20 and ChaCha20 stream ciphers
* scrypt and HKDF
* Deterministic (EC)DSA
* Password-protected PKCS#8 key containers
* Shamir's Secret Sharing scheme
* Random numbers get sourced directly from the OS (and not from a CSPRNG in userspace)
* Simplified install process, including better support for Windows
* Cleaner RSA and DSA key generation (largely based on FIPS 186-4)
* Major clean ups and simplification of the code base
PyCryptodome is not a wrapper to a separate C library like *OpenSSL*.
To the largest possible extent, algorithms are implemented in pure Python.
Only the pieces that are extremely critical to performance (e.g. block ciphers)
are implemented as C extensions.
For more information, see the `homepage`_.
All the code can be downloaded from `GitHub`_.

%package -n python3-%{pypi_name}
Summary:	Cryptographic library for Python3
Provides:	python-%{pypi_name}
Conflicts:      python3-crypto <= 2.6.1
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-cffi
BuildRequires:	gcc

%description -n python3-%{pypi_name}
%{description}

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%doc AUTHORS.rst Changelog.rst README.rst
%license LICENSE.rst
%{python3_sitearch}
%exclude %{python3_sitearch}/Crypto/SelfTest/Publickey/test_vectors/ECC/*.pem
%exclude %{python3_sitearch}/Crypto/SelfTest/Publickey/test_vectors/ECC/*.der
%exclude %{python3_sitearch}/Crypto/SelfTest/Publickey/test_vectors/RSA/*.pem

%changelog
* Tue Jan 09 2024 wangkai <13474090681@163.com> - 3.19.1-1
- Upgrade to version 3.19.1 for fix CVE-2023-52323

* Mon Sep 25 2023 xu_ping<707078654@qq.com> - 3.19.0-1
- upgrade to 3.19.0

* Mon Jul 10 2023 chenzixuan<chenzixuan@kylinos.cn> - 3.18.0-1
- upgrade to 3.18.0

* Tue Feb 28 2023 lvfei <lvfei@kylinos.cn> - 3.17.0-1
- upgrade to 3.17.0

* Wed Dec 14 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 3.16.0-1
- update to 3.16.0

* Fri Jul 29 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 3.15.0-1
- Update to 3.15

* Fri Jul 01 2022 wangjiang <wangjiang37@h-partners.com> - 3.11.0-2
- enable check test suite

* Thu Dec 09 2021 shixuantong<shixuantong@huawei.com> - 3.11.0-1
- update version to 3.11.0

* Thu Jul 22 2021 shixuantong<shixuantong@huawei.com> - 3.9.7-6
- remove gdb from BuildRequires
- Be More specific on BSD and Apache license

* Thu Jun 10 2021 liudabo <liudabo1@huawei.com> - 3.9.7-5
- Refresh the original the original certificate file and delete the insercure certificates

* Thu Mar 04 2021 shixuantong <shixuantong@huawei.com> - 3.9.7-4
- fix Apache license error in spec file

* Mon Oct 26 2020 shixuantong <shixuantong@huawei.com> - 3.9.7-3
- Adding the source code package and spec file
- remove python2-pycryptodome

* Sun Oct 18 2020 shanzhikun <shanzhikun@huawei.com> - 3.9.7-2
- build python2-pycryptodome.

* Wed May 20 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
